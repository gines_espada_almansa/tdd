from django.conf.urls import patterns, url

urlpatterns = patterns('',
    # (.+) es un Capture Group, que captura cualquier caracter hasta el siguiente /
    # El texto capturado pasara a la view como un argumento, el segundo, despues de request.
    # Es 'greedy', por lo que usaremos \d para pillar solo numeros.
    url(r'^(\d)/$', 'lists.views.view_list', name='view_list'),
    url(r'^(\d)/add_item$', 'lists.views.add_item', name='add_item'),
    url(r'^new$', 'lists.views.new_list', name='new_list'),
)
